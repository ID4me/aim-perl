package DenicID::Identifier;

use strict;
use warnings;

use Data::Dumper;

use DenicID::Client;
use DenicID::Crypto;

sub new {
	my ($class, $rsa, $base_url, $kid) = @_;

	my $self = {
		'rsa' => $rsa,
		'kid' => $kid,
		'base_url' => $base_url
	};
	return bless $self, $class;
}

sub create_identity {
	my ($self, $identifier, $locale) = @_;

	$locale = "en"
		if (!$locale);

	my $key_class = new DenicID::Crypto();

	my $headers = {
		'kid' => $self->{'kid'},
		'method' => 'POST',
		'url' => $self->{'base_url'} . '/authz',
		'use' => 'sig'
	};

	my $payload = {
		'identifier' => $identifier,
		'locale' => $locale
	};

	my $denic_id_client = new DenicID::Client();
	my $response = $denic_id_client->send($headers, $payload, $self->{'rsa'});
	return $response;
}

sub confirm_identity {
	my ($self, $url) = @_;

	my $headers = {
		'kid' => $self->{'kid'},
		'method' => 'POST',
		'url' => $url,
		'use' => 'sig'
	};

	my $denic_id_client = new DenicID::Client();
	my $response = $denic_id_client->send($headers, {}, $self->{'rsa'});
	return $response;
}

1;