#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;

use Find::Lib '../lib';

use DenicID::Agent;
use DenicID::Identifier;
use DenicID::Crypto;

use JSON::XS qw/decode_json/;

my $id = shift @ARGV;
die "missing ID"
    if (!$id);

if ($id =~ /^\*\.(.+)/) {
    $id = 'perlhackathon-test-' . time() . '.' . $1;
}

my $key_class = new DenicID::Crypto();
my $rsa_obj = $key_class->generate();

my $kid;
eval {
	my $denic_id_agent = new DenicID::Agent($rsa_obj, 'https://id.staging.denic.de/aim/v1');
	$kid = $denic_id_agent->create_agent_account('perlhackathon' . time(), 'ABC DEF 123456', 'test@test.com');
}; if ($@) {
    print Dumper $@;
    exit;
}

my $denic_id_identifier = new DenicID::Identifier($rsa_obj, 'https://id.staging.denic.de/aim/v1', $kid->{'id'});
my $authz_response;
eval {
	$authz_response = $denic_id_identifier->create_identity($id, 'en');
}; if ($@) {
    print Dumper $authz_response;
    exit;
}

print $key_class->get_acme_challenge($authz_response->{'challenge'}->{'token'}, $rsa_obj) . "\n";

<STDIN>;

$denic_id_identifier->confirm_identity($authz_response->{'challenge'}->{'url'});

print "done\n";
